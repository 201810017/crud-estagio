<header>
  <nav class="navbar navbar-expand-lg navbar-light">
    <div class="collapse navbar-collapse" id="navbar">
      <a href="#" class="navbar-brand">
        <img src="/img/cpds.png" alt="Natureza">
      </a>

      <ul class="navbar-nav">
        <li class="nav-item">
          <a href="/cliente" class="nav-link">Clientes</a>
        </li>
        
        <li class="nav-item">
          <a href="/cliente/create" class="nav-link">Novo Cliente</a>
        </li>
      </ul>
    </div>
  </nav>
</header>