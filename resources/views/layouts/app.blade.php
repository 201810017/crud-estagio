<!DOCTYPE html>
<html lang="pt-br">
    @section('htmlheader')
        <!--inclui a pasta e o arquivo q está dentro -->
        @include('layouts.htmlheader')
        <!-- O links adicionais serão mudados dinamicamente -->
        @yield('links_adicionais') 
        <!-- O scripts adicionais serão mudados dinamicamente -->
        @yield('scripts_adicionais')
    <!--@show faz priorizar o conteúdo de apresentação -->
    @show

    @include('layouts.navbar')
    <body>
        <!-- seção chamanda "conteudo", no qual o conteúdo dela é modifcado dinamicamente -->
        @yield('conteudo')
        @include('layouts.footer')
    </body>
</html>