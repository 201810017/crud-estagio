<head>  
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!--a section q tiver "htmlheader_titulo" consegue injetar o título nele  -->
    <title>@yield('htmlheader_titulo', 'Seu título aqui')</title>
    
    <script type="text/javascript" src="{{asset('plugins/AdminLTE-3.2.0/jquery/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/AdminLTE-3.2.0/bootstrap/js/bootstrap.min.js')}}"></script>

    <!--Icones -->
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>

    <link rel="stylesheet" href="{{asset('plugins/AdminLTE-3.2.0/dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/AdminLTE-3.2.0/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/AdminLTE-3.2.0/overlayScrollbars/css/OverlayScrollbars.min.css') }}">

    <!--CSS da aplicação -->
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">

    <!-- CSS do footer -->
    <link rel="stylesheet" href="{{asset('css/footer.css')}}">

    <!--CSS do Bootstrap -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> -->
</head>