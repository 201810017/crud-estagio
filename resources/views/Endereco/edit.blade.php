@extends('layouts.app')
@section('htmlheader_titulo', 'Editar Endereço')

@section('scripts_adicionais')
<script type="text/javascript" src=" {{asset('plugins/maskedinput/jquery.maskedinput.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready( function($){
        $("#cep").mask("99999-999");
    });    
</script>

@endsection

@section('conteudo')
    <div>
        <section class="content-header">
            <div class="col-12">
                <h2>Atualização do Endereço</h2>
            </div>
        </section>

        <!--Se a Session tem "mensagem" -->
        @if(Session::has('mensagem')) 
            <div class="alert alert-danger alert-dismissible">
                <!-- data-dismiss - clas para fechar o button que abrir sem precisar de nada  -->
                <button type="button" class="close" data-dismiss="alert">x</button>
                <h5><i class="icon fas fa-ban"></i>Atenção!</h5>
                {{Session::get('mensagem')}}
            </div>
        @endif

        <div class="card-body">
            <div id="create-cliente" class="col-md-6 offset-md-3">
                <form action="/endereco/{{$endereco->id}}" method="POST">

                    @csrf
                    @method('PUT')
                    <div class="form-row"> 
                        <!--Guarda o ID do usuário para ser utilizado no request do controller -->
                        <input type="hidden" id="clienteID" name="clienteID" value=" {{ $cliente->id}} ">

                        <div class="form-group col-6">
                            <label>Logradouro</label><br>
                            <input type="text" name="logradouro" class="form-control @error('logradouro') is-invalid @enderror" value="{{ $endereco->logradouro}}" required>
                            
                            @error('logradouro')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="form-group col-6">
                            <label>Bairro</label><br>
                            <input type="text" name="bairro" id="bairro" class="form-control @error('bairro') is-invalid @enderror" value="{{$endereco->bairro}}" required>

                            @error('bairro')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-row"> 
                        <div class="form-group col-6">
                            <label>Número</label>
                            <input type="text" name="numero" class="form-control @error('numero') is-invalid @enderror" value="{{$endereco->numero}}" required>

                            @error('numero')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                            <label>Cidade</label>
                            <input type="text" name="cidade" class="form-control @error('cidade') is-invalid @enderror" value="{{$endereco->cidade}}" required>

                            @error('cidade')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Estado</label>
                            <input type="text" name="estado" class="form-control @error('estado') is-invalid @enderror" value="{{$endereco->estado}}" required>

                            @error('estado')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                            <label>CEP</label>
                            <input type="text" name="cep" id="cep" class="form-control @error('cep') is-invalid @enderror" value="{{$endereco->cep}}" required>

                            @error('cep')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Referência</label>
                            <input type="text" name="referencia" class="form-control @error('referencia') is-invalid @enderror" value="{{$endereco->referencia}}">

                            @error('referencia')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>

                        <div>
                            <button type="submit" class="btn btn-primary float-right" style="margin:32px 0 0 10px;">Salvar</button>
                        </div>         
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection