@extends('layouts.app')
@section('htmlheader_titulo', 'Criar Cliente')

@section('scripts_adicionais')
<script type="text/javascript" src=" {{asset('plugins/maskedinput/jquery.maskedinput.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready( function($){
        $("#cpf").mask("999.999.999-99");
        $("#telefone").mask("(99) 99999-9999");
        $("#data_nascimento").mask("99/99/9999");
    });    
</script>
@endsection

@section('conteudo')
    <!-- <div class="card"> -->
    <div>
        <section class="content-header">
            <div class="col-12">
                <h2>Cadastro do Cliente</h2>
            </div>
        </section>

        <!--Se a Session tem "mensagem" -->
        @if(Session::has('mensagem')) 
            <div class="alert alert-danger alert-dismissible">
                <!-- data-dismiss - clas para fechar o button que abrir sem precisar de nada  -->
                <button type="button" class="close" data-dismiss="alert">x</button>
                <h5><i class="icon fas fa-ban"></i>Atenção!</h5>
                {{Session::get('mensagem')}}
            </div>
        @endif

        <div class="card-body">
            <div id="create-cliente" class="col-md-6 offset-md-3">
                <form action="/cliente" method="POST">
                    
                    @csrf
                    <div class="form-row"> 
                        <div class="form-group col-6">
                            <label>Nome</label><br>
                            <input type="text" name="nome" placeholder="Ex: João Santos" class="form-control @error('nome') is-invalid @enderror" value="{{ old('nome') }}" required>
                            
                            @error('nome')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="form-group col-6">
                            <label>E-mail</label><br>
                            <input type="email" name="email" placeholder="Ex: joao@gmail.com" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>CPF</label>
                            <input type="text" name="cpf" placeholder="Ex: 123.456.789-10" id="cpf" class="form-control @error('cpf') is-invalid @enderror" value="{{ old('cpf') }}" required>

                            @error('cpf')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group col-6">
                            <label>Data de Nascimento</label>
                            <input type="text" name="data_nascimento" placeholder="Ex: 10/10/2010" id="data_nascimento" class="form-control @error('data_nascimento') is-invalid @enderror" value="{{ old('data_nascimento') }}" required>

                            @error('data_nascimento')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                        

                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Sexo</label><br>
                            <select name="sexo" id="sexo" class="form-control" required>
                                <option value="">Selecione</option>
                                <option value="Masculino">Masculino</option>
                                <option value="Feminino">Feminino</option>
                            </select>
                        </div>

                        <div class="form-group col-6">
                            <label>Telefone</label>
                            <input type="text" name="telefone" placeholder="Ex: (73) 98812-3456" id="telefone" class="form-control @error('telefone') is-invalid @enderror" value="{{ old('telefone') }}" required>

                            @error('telefone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div>  
                        <button type="submit" class="btn btn-primary float-right" style="margin:32px 0 0 10px;">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection