<!-- estende tudo do arquivo "app.blade": header, footer. layouts (pasta) -> app (arq)   -->
@extends('layouts.app')
@section('htmlheader_titulo', 'Crud Estágio')

@section('links_adicionais')
  <link rel="stylesheet" href="{{asset('plugins/AdminLTE-3.2.0/DataTables/datatables.min.css')}}">
@endsection

@section('scripts_adicionais')
  <script src="{{asset('plugins/AdminLTE-3.2.0/DataTables/datatables.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('js/listar_clientes.js') }}"></script>
  <link rel="stylesheet" href="{{asset('css/styles.css')}}">
@endsection

@section('conteudo')
  <div class="card">

    <!--Se a Session tem "mensagem" -->
    @if(Session::has('mensagem'))
      <div class="alert alert-info alert-dismissible">
          <!-- data-dismiss - clas para fechar o button que abrir sem precisar de nada  -->
        <button type="button" class="close" data-dismiss="alert">x</button>
        {{Session::get('mensagem')}}
      </div>
    @endif

    <h2>Clientes</h2><br>

    <div class="card-body">
      <div class="container">
          
          <a href="/cliente/create" class="btn btn-outline-primary col-2"><b>Cadastro de Cliente</b></a>
          <br><br><br>

          <table id="clientes" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>CPF</th>
                <th>Data de Nascimento</th>
                <th>Sexo</th>
                <th>Telefone</th>
                <th>Endereço</th>
                <th>Ação</th>
              </tr>
            </thead>
            
            <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
@endsection