<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# Projeto de Treinamento - CRUD de Clientes

## Sobre o projeto
Este sistema faz o CRUD (Create, Read, Update, Delete) de clientes. Ao cadastrar um cliente, inicialmente ele será salvo sem endereço. Na tabela que lista os clientes, o usuário pode visualizar os dados dos clientes cadastrados e pode visualizar a opção "Cadastrar" na coluna de endereço dos usuários que ainda não possuem o mesmo. Após cadastrar um endereço, a opção "Cadastrar" é atualizada para "Editar". Na regra de negócios desse sistema, o endereço não pode ser deletado, apenas criado ou atualizado. Além disso, cada cliente cadastrado pode possuir apenas um endereço.

Este sistema foi desenvolvido na linguagem PHP utlizando o framework Laravel. Para o seu desenvolvimento, foram utilizados alguns recursos como: MaskedInput para formatação dos campos, jQuery DataTable e Bootstrap para estilização de layout.

## Tecnologias utilizadas
* PHP e Laravel
* Banco de Dados Postgres
* Docker

## Layout do sistema
![Tabela de clientes](imagens_sistema/cliente_lista.png)
![Cadastro de cliente](imagens_sistema/cliente_create.png)
![Atualização de cliente](imagens_sistema/cliente_update.png)
![Cadastro de endereço](imagens_sistema/endereco_create.png)

## Modelagem do Banco de Dados
O banco de dados possui duas tabelas: Cliente e Endereço.

![Tabela de cliente](imagens_sistema/tabela_cliente.png)
![Tabela de endereço](imagens_sistema/tabela_endereco.png)

## Autor
Raul Santos Gonçalves
