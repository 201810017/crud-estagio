<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = ['fk_endereco, nome, email, cpf, data_nascimento, sexo, telefone'];    //Colunas do BD

    public function fk_endereco() {   
        //hasMany é o tipo de relacionamento entre as tabelas
        return $this->belongsTo('App\Endereco','fk_endereco');
    }
}
