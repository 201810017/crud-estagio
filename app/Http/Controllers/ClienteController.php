<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;         //Model Cliente
use DataTables;
use Redirect;
//use Illuminate\Support\Facades\Session;
use Session;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Cliente.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Cliente.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)         //Salva os dados no BD
    {
        try {
            $cliente = new Cliente();
            $cliente->nome = $request->nome;
            $cliente->email = $request->email;
            $cliente->cpf = $request->cpf;
            $cliente->data_nascimento = $request->data_nascimento;
            $cliente->sexo = $request->sexo;
            $cliente->telefone = $request->telefone;

            //Caso tenha várias operações dentro do transaction (neste caso tem apenas o save) e uma delas der erro, desfaz todas operações. Ex: update ok e delete erro -> desfaz o update.
            DB::transaction(function() use ($cliente) {
                $cliente->save();
            });
            
            //Exibe uma msg qnd voltar para a tela
            Session::flash('mensagem','Cliente cadastrado!');
            return Redirect::to('/cliente');
        } 
        catch (\Exception $error) {
            Session::flash('mensagem', 'Deu erro!');
            //Back faz retornar tds os valores para o input
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = Cliente::get();
            return Datatables::of($cliente)
            ->editColumn('fk_endereco', function ($cliente) {

                //Se o cliente não possui endereço, ele faz o cadastro
                if ($cliente->fk_endereco == null || $cliente->fk_endereco == '') {
                    return '
                    <div class="btn-group btn-group-sm">
                        <a href="/cliente/'.$cliente->id.'/endereco"
                            class="btn btn-info"
                            title="Cadastrar Endereço" data-toggle="tooltip">Cadastrar
                            <i class="bi bi-building"></i>
                        </a>
                    </div>';
                }
                else {          //Caso o cliente já possua um endereço, ele pode apenas editá-lo
                    return '
                    <div class="btn-group btn-group-sm">
                        <a href="/cliente/'.$cliente->id.'/endereco/edit"
                            class="btn btn-info"
                            title="Editar Endereço" data-toggle="tooltip">Editar
                            <i class="bi bi-building"></i>
                        </a>
                    </div>';
                }
            })
            ->editColumn('acao', function ($cliente) {
                return '
                    <div class="btn-group btn-group-sm">
                        <a href="/cliente/'.$cliente->id.'/edit"
                            class="btn btn-info"
                            title="Editar" data-toggle="tooltip">
                            <i class="fas fa-pencil-alt"></i>
                        </a>
                        <a href="#"
                            class="btn btn-danger btnExcluir"
                            data-id="'.$cliente->id.'"
                            title="Excluir" data-toggle="tooltip">
                            <i class="fas fa-trash"></i>
                        </a>
                    </div>';
            })
            ->escapeColumns([0])
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::find($id); 
        return view('Cliente.edit', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $cliente = Cliente::find($id);
            $cliente->nome = $request->nome;
            $cliente->email = $request->email;
            $cliente->cpf = $request->cpf;
            $cliente->data_nascimento = $request->data_nascimento;
            $cliente->sexo = $request->sexo;
            $cliente->telefone = $request->telefone;

            DB::transaction(function() use ($cliente) {
                $cliente->save();
            });
            
            Session::flash('mensagem','Cliente atualizado!');
            return Redirect::to('/cliente');
        } 
        catch (\Exception $error) {
            Session::flash('mensagem', 'Deu erro!');
            return back()->withInput();
        }
       //return Redirect::to('/cliente/'.$cliente->id.'/edit');    //http://crud-estagio.local/cliente/{id}/edit
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $cliente = Cliente::find($id);
            $cliente->delete();
            return response()->json(array('status' => "OK"));
        }catch (\Exception  $erro) {
            return response()->json(array('erro' => "ERRO"));
        }
        //return Redirect::to('/cliente');
    }
}
