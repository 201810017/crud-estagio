<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Endereco;         //Model Endereco
use App\Cliente;          //Model Cliente
use DataTables;
use Redirect;
use Session;
use Illuminate\Support\Facades\DB;

class EnderecoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Endereco.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $endereco = new Endereco();
            $endereco->logradouro = $request->logradouro;
            $endereco->bairro = $request->bairro;
            $endereco->numero = $request->numero;
            $endereco->cidade = $request->cidade;
            $endereco->estado = $request->estado;
            $endereco->cep = $request->cep;
            $endereco->referencia = $request->referencia;

            //Recupera o Cliente dono do endereço que está sendo cadastrado
            $cliente = Cliente::find($request->clienteID);  
            
            DB::transaction(function() use ($endereco, $cliente) {
                $endereco->save();
        
                $cliente->fk_endereco = $endereco->id;     //Salva o ID do endereço no cliente
                $cliente->save();                          //Atualiza o cliente com a nova fk
            });

            Session::flash('mensagem','Endereço cadastrado!');
            return Redirect::to('/cliente');
        } 
        catch (\Exception $error) {
            Session::flash('mensagem', 'Deu erro!');
            return back()->withInput();     //Back faz retornar tds os valores para o input
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $endereco = Endereco::find($id); 
        return view('Endereco.edit', compact('endereco'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $endereco = Endereco::find($id);
            $endereco->logradouro = $request->logradouro;
            $endereco->bairro = $request->bairro;
            $endereco->numero = $request->numero;
            $endereco->cidade = $request->cidade;
            $endereco->estado = $request->estado;
            $endereco->cep = $request->cep;
            $endereco->referencia = $request->referencia;

            DB::transaction(function() use ($endereco) {
                $endereco->save();
            });

            Session::flash('mensagem','Endereço atualizado!');
            return Redirect::to('/cliente');
        } 
        catch (\Exception $error) {
            Session::flash('mensagem', 'Deu erro!');
            return back()->withInput();     //Back faz retornar tds os valores para o input
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
