<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestCliente extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;    //Alturização para usar o request
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()//nome, email, cpf, data_nascimento, telefone'
    {
        if($this->method() == "PUT") {
            return [
                'nome' => ['required'],
                'email' => ['required'],
                'cpf' => ['required'],
                'data_nascimento' => ['required'],
                'sexo' => ['required'],
                'telefone' => ['required']
            ];
            
        }
        else if($this->method() == "POST") {
            return[
             'nome' => ['required', 'string', 'max:255','regex:/^([a-zA-Zà-úÀ-Ú]|-|_|\s)+$/']
            ];
         }
    }
}
