<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestEndereco extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //Para n precisar criar 2 arq para o request, add duas verificação se o método é PUT ou POST 
        if($this->method() == "PUT") {
            return [
                'logradouro' => ['required'],
                'bairro' => ['required'],
                'numero' => ['required'],
                'cidade' => ['required'],
                'estado' => ['required'],
                'cep' => ['required']
            ];
            
        }
        else if($this->method() == "POST") {
            return[
             'cidade' => ['required', 'string', 'max:255','regex:/^([a-zA-Zà-úÀ-Ú]|-|_|\s)+$/'],
             'estado' => ['required','string','max:255','regex:/^([a-zA-Zà-úÀ-Ú]|-|_|\s)+$/']
            ];
         }   
    }
}
