<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    //Colunas do BD
    protected $fillable = ['logradouro, bairro, numero, cidade, estado, cep, referencia'];     
}
