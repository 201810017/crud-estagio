<?php

use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\EventController;
use App\Cliente;         //Model Cliente
use App\Endereco;   

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cliente/create', function () {
    return view('Cliente.create');
});

//Primeiro parâmetro é a rota da URL e o segundo é o arquivo retornado
Route::resource('cliente', 'ClienteController');   //Acessa a função index do Controller
Route::resource('endereco', 'EnderecoController'); 

Route::get('/cliente/{id}/endereco', function ($id = null) {
    $cliente = Cliente::find($id);
    return view('Endereco.create', ['cliente' => $cliente]);    //Leva o obj cliente para a pág de Endereço
});

Route::get('/cliente/{id}/endereco/edit', function ($id = null) {
    $cliente = Cliente::find($id);
    $endereco = Endereco::find($cliente->fk_endereco);
    return view('Endereco.edit', ['cliente' => $cliente, 'endereco' => $endereco ]);    //Leva o obj cliente para a pág de Endereço
});

//Route::get('/cliente/create', [ClienteController::class, 'create'])      //Acessa a função create

