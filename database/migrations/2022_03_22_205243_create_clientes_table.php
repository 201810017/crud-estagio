<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('fk_endereco')->nullable();
            //Cliente acessa Endereço
            $table->foreign('fk_endereco')->references('id')->on('enderecos')->onDelete('cascade');
            
            $table->string('nome');
            $table->string('email');
            $table->string('cpf', 14);
            $table->string('data_nascimento');
            $table->string('sexo');
            $table->string('telefone')->nullable();     //nullable permite o campo ser nulo

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
